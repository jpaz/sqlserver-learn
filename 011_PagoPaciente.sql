USE CentroMedico
GO
--PAGO PACIENTE
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'PagoPaciente'
)
CREATE TABLE PagoPaciente (
    id_pago INT NOT NULL,
    id_paciente INT NOT NULL,
    id_turno INT NOT NULL,
    CONSTRAINT PK_PAGO_PACIENTE PRIMARY KEY(id_pago, id_paciente, id_turno)
)