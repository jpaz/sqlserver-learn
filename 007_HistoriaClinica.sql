USE CentroMedico
GO
-- HISTORIA CLINICA ID TYPE
IF NOT EXISTS (
    SELECT [name]
    FROM sys.types
    WHERE [name] = N'HistoriaId'       
)
CREATE TYPE HistoriaId FROM INT NOT NULL;

GO
-- HISTORIA CLINICA
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'Historia'       
)
CREATE TABLE Historia
(
    id HistoriaId IDENTITY(1, 1),
    fecha DATETIME NOT NULL,
    observacion VARCHAR(2000) NOT NULL,
    CONSTRAINT PK_ID_HISTORIA PRIMARY KEY(id)
);

GO
-- RELACION HISTORIA/PACIENTE
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'HistoriaPaciente'
)
CREATE TABLE HistoriaPaciente
(
    id_historia HistoriaId,
    id_paciente PacienteId,
    id_medico MedicoId,
    CONSTRAINT PK_HISTORIA_PACIENTE PRIMARY KEY(id_historia, id_paciente, id_medico),
    CONSTRAINT FK_PACIENTE FOREIGN KEY(id_paciente) REFERENCES Paciente(id),
    CONSTRAINT FK_MEDICO FOREIGN KEY(id_medico) REFERENCES Medico(id)
);