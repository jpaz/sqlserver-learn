USE CentroMedico
GO
-- PACIENTE ID TYPE
IF NOT EXISTS (
    SELECT [name]
    FROM sys.types
    WHERE [name] = N'PacienteId'
)
BEGIN

CREATE TYPE PacienteId FROM INT NOT NULL;

END;

GO
-- PACIENTE
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'Paciente'
)
BEGIN

CREATE TABLE Paciente
(
    id PacienteId IDENTITY(1, 1),
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    nacimiento DATE NOT NULL,
    domicilio VARCHAR(50) NOT NULL,
    id_pais CHAR(3) NOT NULL,
    telefono VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
    observacion VARCHAR(1000),
    CONSTRAINT PK_ID_PACIENTE PRIMARY KEY(id),
    CONSTRAINT FK_PAIS FOREIGN KEY(id_pais) REFERENCES Pais(id)
);

END;