USE CentroMedico
GO
-- TURNO ESTADO
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'TurnoEstado'
)
CREATE TABLE TurnoEstado
(
    id SMALLINT NOT NULL,
    descripcion VARCHAR(50) NOT NULL,
    CONSTRAINT PK_ID_TURNO_ESTADO PRIMARY KEY(id)
)

-- INSERTAR TURNO ESTADO
INSERT INTO TurnoEstado VALUES(0, 'Pendiente')
INSERT INTO TurnoEstado VALUES(1, 'Realizado')
INSERT INTO TurnoEstado VALUES(2, 'Cancelado')

-- TURNO
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'Turno'
)
CREATE TABLE Turno
(
    id INT IDENTITY NOT NULL,
    fecha DATETIME NOT NULL,
    estado SMALLINT NOT NULL,
    observacion VARCHAR(300),
    CONSTRAINT PK_ID_TURNO PRIMARY KEY (id)
)

-- RELACION TURNO/PACIENTE 
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'TurnoPaciente'
)
CREATE TABLE TurnoPaciente
(
    id_turno INT NOT NULL,
    id_paciente INT NOT NULL,
    id_medico INT NOT NULL,
    CONSTRAINT PK_TURNO_PACIENTE PRIMARY KEY(id_turno, id_paciente, id_medico)
)