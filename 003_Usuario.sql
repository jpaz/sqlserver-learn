USE CentroMedico
GO
-- USUARIO
IF NOT EXISTS (
    SELECT [name]
    FROM sys.types
    WHERE [name] = N'UsuarioId'
)

CREATE TYPE UsuarioId FROM INT NOT NULL;