USE CentroMedico
GO
-- PAIS
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'Pais'
)
CREATE TABLE Pais
(
    id char(3) NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    CONSTRAINT PK_ID_PAIS PRIMARY KEY(id)
);