USE CentroMedico
GO
--PAGO
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'Pago'
)
CREATE TABLE Pago (
    id INT IDENTITY(1,1) NOT NULL,
    concepto TINYINT NOT NULL,
    fecha DATETIME NOT NULL,
    monto MONEY NOT NULL,
    estado TINYINT NOT NULL,
    observacion VARCHAR(1000),
    CONSTRAINT PK_ID_PAGO PRIMARY KEY(id)
)