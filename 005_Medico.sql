USE CentroMedico
GO
-- MEDICO ID TYPE
IF NOT EXISTS (
    SELECT [name]
    FROM sys.types
    WHERE [name] = N'MedicoId'
)
BEGIN

CREATE TYPE MedicoId FROM INT NOT NULL;

END;

GO
-- MEDICO
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'Medico'
)
BEGIN

CREATE TABLE Medico
(
    id MedicoId IDENTITY(1, 1),
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    nacimiento DATE NOT NULL,
    domicilio VARCHAR(50) NOT NULL,
    id_pais CHAR(3) NOT NULL,
    telefono VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
    CONSTRAINT PK_ID_MEDICO PRIMARY KEY(id),
    CONSTRAINT FK_PAIS FOREIGN KEY(id_pais) REFERENCES Pais(id)
);

END;