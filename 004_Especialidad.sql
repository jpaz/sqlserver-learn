USE CentroMedico
GO
-- ESPECIALIDAD
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'Especialidad'
)
CREATE TABLE Especialidad (
    id INT IDENTITY(1,1) NOT NULL,
    descripcion VARCHAR(100) NOT NULL,
    CONSTRAINT PK_ID_ESPECIALIDAD PRIMARY KEY(id)
);