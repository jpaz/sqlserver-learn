USE CentroMedico
GO
-- CONCEPTO
IF NOT EXISTS (
    SELECT [name]
    FROM sys.tables
    WHERE [name] = N'Concepto'
)
CREATE TABLE Concepto (
    id TINYINT IDENTITY(1,1) NOT NULL,
    descripcion VARCHAR(100) NOT NULL,
    CONSTRAINT PK_CONCEPTO PRIMARY KEY(id)
)